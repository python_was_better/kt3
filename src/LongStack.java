import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class LongStack {

   private LinkedList<Long> stack;

   LongStack() {
      stack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clonedStack = new LongStack();
      clonedStack.stack = new LinkedList<>(this.stack);
      return clonedStack;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (long a) {
      stack.add(a);
   }

   public long pop() {
      if (stackSize() == 0) {
         throw new RuntimeException("Stack underflow");
      }
      return stack.remove(stackSize() - 1);
   }

   public void op (String s) {
      stack.push(opSwitch(s, pop(), pop()));
   }

   public long tos() {
      if (stackSize() == 0) {
         throw new RuntimeException("Stack underflow");
      }
      return stack.getLast();
   }

   @Override
   public boolean equals (Object o) {
      return o instanceof LongStack && ((LongStack) o).stack.equals(this.stack);
   }

   @Override
   public String toString() {
      List<Long> stack = new LinkedList<>(this.stack);
      return stack.stream().map(String::valueOf).collect(joining(" "));
   }

   public static long interpret (String pol) {
      if (pol == null || pol.strip().isEmpty()) {
         throw new RuntimeException("Empty or null expression");
      }

      LongStack longStack = new LongStack();
      String[] splitStr = pol.split(" ");

      for (String str : splitStr) {
         String strippedStr = str.strip();
         if (strippedStr.length() > 0) {
            try {
               if ("+ - * / SWAP ROT".contains(strippedStr)) {
                  long val1 = longStack.pop();
                  long val2 = longStack.pop();
                  if (strippedStr.equals("SWAP")) {
                     longStack.push(val1);
                     longStack.push(val2);
                  } else if (strippedStr.equals("ROT")) {
                     long val3 = longStack.pop();
                     longStack.push(val2);
                     longStack.push(val1);
                     longStack.push(val3);
                  } else {
                     longStack.push(longStack.opSwitch(strippedStr, val1, val2));
                  }
               } else {
                  longStack.push(tryParseLong(strippedStr));
               }
            } catch (RuntimeException e) {
               throw new RuntimeException(e.getMessage() + String.format(" in expression \"%s\"", pol));
            }
         }
      }

      if (longStack.stackSize() != 1) {
         throw new RuntimeException(String.format("Too many numbers in expression \"%s\"", pol));
      }
      return longStack.pop();
   }

   private long opSwitch(String op, long val1, long val2) {
      switch (op) {
         case "+":
            return val1 + val2;
         case "-":
            return val2 - val1;
         case "*":
            return val1 * val2;
         case "/":
            if (val2 == 0L) {
               throw new ArithmeticException("Division by zero");
            }
            return val2 / val1;
         default:
            throw new RuntimeException(format("Unknown operator: \"%s\"", op));
      }
   }

   private static long tryParseLong(String value) {
      try {
         return Long.parseLong(value);
      } catch (NumberFormatException e) {
         throw new RuntimeException(format("Illegal symbol: \"%s\"", value));
      }
   }

   private int stackSize() {
      return stack.size();
   }
}